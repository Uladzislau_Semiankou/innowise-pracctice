// Функция принимает массив meetups,
//     и возвращает суммарное количество человек, находящихся на активных митапах
//
// membersOnActiveMeetups(meetups); // 1500
//
// Пример:
//     const meetups = [
//         { name: 'JavaScript', isActive: true, members: 100 },
//         { name: 'Angular', isActive: true, members: 900 },
//         { name: 'Node', isActive: false, members: 600 },
//         { name: 'React', isActive: true, members: 500 },
//     ];
// membersOnActiveMeetups(meetups); // 1500

const membersOnActiveMeetups = (arr) => {
    const result =  arr.reduce((acc, {isActive, members}) => isActive ? acc + members : acc, 0)
    return result;
}

export { membersOnActiveMeetups }
