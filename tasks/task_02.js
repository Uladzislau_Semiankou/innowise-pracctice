// Функция принимает 2 массива.
//     Возвращает новый массив, который состоит только из тех элементов,
//     которые встретились в одном массиве, но не встретились в другом
//
// console.log(arrayDiff([1, 2, 3], [1, 2, 4])); -> [3, 4]
// console.log(arrayDiff([1, 3, 3, 4], [1, 3, '4'])); -> [4, '4']


const arrayDiff = (arrFirst, arrSecond) => {
    let arrFirstFiltred = arrFirst.filter((elem) => !arrSecond.includes(elem));
    let arrSecondFiltred = arrSecond.filter((elem) => !arrFirst.includes(elem));
    return [...arrFirstFiltred, ...arrSecondFiltred];
}

export { arrayDiff };
