// Напишите функцию unique(arr), которая возвращает массив, содержащий только уникальные элементы arr.
//
//     Например:
//
// let strings = [‘aaa’, ‘aaa’, ‘zzz’, ‘xxx’, ‘aaa’, ‘bbb’, ‘aaa’,  ‘xxx’, ‘ccc’];
//
// alert( unique(strings) ); // [‘aaa’, ‘zzz’, ‘xxx’, ‘bbb’, ‘ccc’]

const unique = (arr) => {
    return arr.reduce((acc, curr) => {
        return !acc.includes(curr) ? acc + ' ' + curr: acc;
    }).split(' ')
}

export { unique }
