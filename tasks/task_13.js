// camelize("background-color") == 'backgroundColor';
// camelize("list-style-image") == 'listStyleImage';
// camelize("-webkit-transition") == 'WebkitTransition';

const camelize = (str) => {
    const arrStr = str.split('-')
    return arrStr.map((elem, index) => index !== 0 ? elem[0].toUpperCase() + elem.slice(1) : elem).join('');
}

export { camelize }
