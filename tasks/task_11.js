// Каррирование
// add(4)(3)(1) => 8

const add = (paramFirst) => {
    return (paramSecond) => {
        return (paramThird) => {
            return paramFirst + paramSecond + paramThird;
        }
    }
}

function sum(a) {

    let currentSum = a;

    function newFunc(b) {
        if (b) {
            currentSum += b;
            return newFunc;
        } else {
            return currentSum;
        }
    }

    newFunc.toString = () => currentSum;
    newFunc.valueOf = () => currentSum;
    newFunc[Symbol.toPrimitive] = () => currentSum;

    return newFunc;
}


export { sum }
