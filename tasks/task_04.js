// Функция принимает 2 массива, и возвращает массив объединенных значений,
//     без дублирования
//
// console.log(union([5, 1, 2, 3, 3], [4, 3, 2])); -> [5, 1, 2, 3, 4]
// console.log(union([5, 1, 3, 3, 4], [1, 3, 4])); -> [5, 1, 3, 4]

const union = (arrFirst, arrSecond) => {
    return [...new Set ([... arrFirst, ... arrSecond])];
}

export { union };
