// Напишите функцию sumTo(n), которая вычисляет сумму чисел 1 + 2 + ... + n.

const sumTo = (num) => {
    return num === 0 ? num : num + sumTo(num - 1);
}

export { sumTo }
