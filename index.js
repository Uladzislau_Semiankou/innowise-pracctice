// import { anyDeclarative, anyImperative } from './tasks/task_01.js';
// import { arrayDiff } from './tasks/task_02.js';
// import { forEachRight } from './tasks/task_03.js'
// import  { union } from "./tasks/task_04.js";
// import { createGenerator } from "./tasks/task_05.js";
// import { without } from "./tasks/task_06.js";
// import { indexOfAll } from "./tasks/task_07.js";
// import { membersOnActiveMeetups } from "./tasks/task_08.js";
// import { factory } from "./tasks/task_09.js";
// import { obj } from "./tasks/task_10.js";
// import { sum } from "./tasks/task_11.js";
// import { getMaxSubSum } from "./tasks/task_12.js";
// import { camelize } from "./tasks/task_13.js";
// import { filterRange } from "./tasks/task_14.js";
// import { unique } from "./tasks/task_15.js";
import { sumTo } from "./tasks/task_16.js";
import { spinWords } from "./tasks/task_17.js";
import { filter_list } from "./tasks/task_18.js";
import { squareNum } from "./tasks/task_19.js";
import { persistence } from "./tasks/task_20.js";


// console.log('Index.js');
//Task_01
// console.log(anyDeclarative([0, 1, 2, 0], x => x >= 2));
// console.log(anyDeclarative([0, 0, 1, 0]));
// console.log(anyDeclarative([0, 0, 0, 0]));
//
// console.log(anyImperative([0, 1, 2, 0], x => x >= 2));
// console.log(anyImperative([0, 0, 1, 0]));
// console.log(anyImperative([0, 0, 0, 0]));

//Task_02
// console.log(arrayDiff([1, 2, 3], [1, 2, 4]));
// console.log(arrayDiff([1, 3, 3, 4], [1, 3, '4']));

//Task_03
// forEachRight([1, 2, 3, 4], val => console.log(val));

//Task 04
// console.log(union([5, 1, 2, 3, 3], [4, 3, 2]));
// console.log(union([5, 1, 3, 3, 4], [1, 3, 4]));

//Task 05
// const generator = createGenerator([1, '6', 3, 2]);
// console.log(generator.next())
// console.log(generator.next())
// console.log(generator.next())
// console.log(generator.next())
// console.log(generator.next())
// console.log(generator.next())

// console.log(without([2, 1, 2, 3], 1, 2))
// console.log(without([2, 1, 10, 20, 5], 1, 2, 5))

// console.log(indexOfAll([1, 2, 3, 1, 2, 3], 1));
// console.log(indexOfAll([1, 2, 3], 4));

// console.log(sum(2)(3)(5))
//
//     const meetups = [
//         { name: 'JavaScript', isActive: true, members: 100 },
//         { name: 'Angular', isActive: true, members: 900 },
//         { name: 'Node', isActive: false, members: 600 },
//         { name: 'React', isActive: true, members: 500 },
//     ];
// console.log(membersOnActiveMeetups(meetups)) // 1500

// const obj = factory(12, 23, 'myFunc');
//
// console.log(obj.x, obj.y, obj.myFunc());

// console.log(`Name: ${obj}`); 		// Name: Obj-name
// console.log(+obj);            		// 0
// console.log(obj + 10);        		// 10

// console.log(getMaxSubSum([-1, 2, 3, -9]));
// console.log(getMaxSubSum([2, -1, 2, 3, -9]));
// console.log(getMaxSubSum([-1, 2, 3, -9, 11]));
// console.log(getMaxSubSum([-2, -1, 1, 2]));
// console.log(getMaxSubSum([100, -9, 2, -3, 5]));
// console.log(getMaxSubSum([1, 2, 3]));

// console.log(camelize("background-color"))
// console.log(camelize("list-style-image"))
// console.log(camelize("-webkit-transition"))

// let arr = [5, 3, 8, 1];
//
// let filtered = filterRange(arr, 1, 4);
//
// console.log( filtered ); // 3,1
//
// let strings = ['aaa', 'aaa', 'zzz', 'xxx', 'aaa', 'bbb', 'aaa',  'xxx', 'ccc'];
//
// console.log( unique(strings) ); // [‘aaa’, ‘zzz’, ‘xxx’, ‘bbb’, ‘ccc’]

console.log(sumTo(10));

console.log(spinWords( "Hey fellow warriors", 5 ));
console.log(spinWords( "This is a test", 10));
console.log(spinWords( "This is another test", 3 ));

console.log(filter_list([1,2,'a','b']));
console.log(filter_list([1,'a','b',0,15]));
console.log(filter_list([1,2,'aasf','1','123',123]));


console.log(squareNum(9119));


console.log(persistence(39))
console.log(persistence(999))
console.log(persistence(4));
